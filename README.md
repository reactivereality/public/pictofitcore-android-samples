# PictofitCore Android Sample Collection
>💡 Check out the [online documentation](https://docs.pictofit.com/android/Working%20version/) for detailed setup instructions.</br>

This repository contains a collection of several example projects, which can be found in the `samples/` directory.

|Sample|Description|
|------|-----------|
[ARView](samples/ARView)|Use the ARView to place and interact with Renderables in the scene|
[OrbitViewerLayout](samples/OrbitViewerLayout)|Use of the OrbitViewerLayout to provide a preview of a 3D Garment|
[SceneLayout](samples/SceneLayout)|2D scenes in combination with 2D Avatars & Garments|
[SlotLayout](samples/SlotLayout)|Organize a 2D RenderView with slots|
[TryOn2D](samples/TryOn2D)|A simple 2D try on example app|
[OffscreenRendering](samples/OffscreenRendering)|Render a scene without displaying it while doing so|
[SizeRecommendation2D](samples/SizeRecommendation2D)|Compute and visualize a Size Recommendation for 2D Assets|

## LFS Support
Large files (like garment, images and avatars assets) are stored with a LFS (large file format).
We use the “Large File Storage” (LFS) feature of GitLab in our repositories to avoid messing them up with large binary files.
To use this feature, make sure to install the according client for your platform. See https://gitlab.com/help/topics/git/lfs/index for instructions on working with this feature. 
Repositories that are already configured correctly can be used the same way as repositories without LFS as soon as you have installed the LFS client.
