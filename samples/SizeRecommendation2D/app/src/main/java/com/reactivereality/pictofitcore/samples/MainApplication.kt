package com.reactivereality.pictofitcore.samples

import android.app.Application
import com.reactivereality.pictofitcore.PictofitCore

class MainApplication : Application() {
  override fun onCreate() {
    super.onCreate()
    PictofitCore.init(this)
  }
}