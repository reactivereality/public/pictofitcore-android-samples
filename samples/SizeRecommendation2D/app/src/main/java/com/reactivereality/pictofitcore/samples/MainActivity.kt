package com.reactivereality.pictofitcore.samples

import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRAvatar
import com.reactivereality.pictofitcore.RRGarment
import com.reactivereality.pictofitcore.objectdatafileformat.RRLargeObjectDataProviderDirectory
import com.reactivereality.pictofitcore.objectdatafileformat.RRLargeObjectDataProviderDirectoryConfig
import com.reactivereality.pictofitcore.rendering.layouts.RRUserPhotoLayout
import com.reactivereality.pictofitcore.rendering.renderables.RRAvatarRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRGarmentRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRSizeRecommendationRenderable
import com.reactivereality.pictofitcore.utils.RRUtils
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

class MainActivity : AppCompatActivity() {
  private lateinit var renderView: RRGLRenderView
  private lateinit var sizeLabel: TextView

  private lateinit var sizeRecommendationRenderable: RRSizeRecommendationRenderable

  private var sizeIndex: Int = 0
  private var sizeNames = arrayListOf<String>()

  @RequiresApi(Build.VERSION_CODES.O)
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    renderView = findViewById(R.id.renderView)
    sizeLabel = findViewById(R.id.textCurrentSize)
    findViewById<Button>(R.id.btnChangeSize).setOnClickListener {
      sizeIndex = (sizeIndex + 1) % sizeNames.count()
      updateSizeVisualization()
    }

    val largeObjectDataProvider = prepareLargeDataProvider()

    val avatar = RRAvatar.createFromAsset("alex.avatar")!!
    val avatarRenderable = RRAvatarRenderable.createWithAvatar(avatar)!!
    avatarRenderable.backgroundHidden = true

    val garment = RRGarment.createFromAsset("pants_w_size_table.garment")!!
    val garmentRenderable = RRGarmentRenderable.createWithGarment(garment)!!

    val layout = RRUserPhotoLayout.create()!!
    layout.avatarRenderable = avatarRenderable
    layout.garmentRenderables = arrayListOf(garmentRenderable)
    renderView.setLayout(layout)

    sizeRecommendationRenderable = RRSizeRecommendationRenderable.create()!!
    sizeRecommendationRenderable.setLargeObjectDataProvider(largeObjectDataProvider)
    garmentRenderable.sizeRecommendationRenderable = sizeRecommendationRenderable

    garment.garmentSizeTable!!.sizeDescriptions.keys.forEach { sizeName ->
      sizeNames.add(sizeName)
    }

    updateSizeVisualization()
  }

  private fun updateSizeVisualization() {
    sizeRecommendationRenderable.setCurrentSizeName(sizeNames[sizeIndex])
    sizeLabel.text = "Current Size: " + sizeNames[sizeIndex]
  }

  @RequiresApi(Build.VERSION_CODES.O)
  private fun prepareLargeDataProvider(): RRLargeObjectDataProviderDirectory {
    val externalFilesDir = getExternalFilesDir(null)
    val manager = assets

    val dataPath = externalFilesDir.toString() + "/"
    val configFilePath = dataPath + "large_object_data_provider_config.bin"

    try {
      var data: ByteArray? = null
      data = RRUtils.loadData(manager, "default_female.bin")
      Files.write(Paths.get(dataPath + "default_female.bin"), data)
      data = RRUtils.loadData(manager, "default_male.bin")
      Files.write(Paths.get(dataPath + "default_male.bin"), data)
      data = RRUtils.loadData(manager, "large_object_data_provider_config.json")
      Files.write(Paths.get(configFilePath), data)
    } catch (e: IOException) {
      e.printStackTrace()
    }

    val config = RRLargeObjectDataProviderDirectoryConfig.createFromConfigFile(configFilePath)!!
    return RRLargeObjectDataProviderDirectory.createWithConfig(config, dataPath)!!
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}