# SceneLayout Sample Application

This sample shows how to display 2D try-ons in a 3D scene where camera orientation changes based on device rotation.

This sample application includes code snippets showing how to implement the following tasks using the PictofitCore Android SDK:
* Loading and rendering predefined scenes from file using the `RRSceneLayout` class
* Switching between different predefined scenes
* Rendering a 2D try-on with 2 different garments

## Screenshots
<table><tr>
<td> <img src="screenshots/screenshot1.jpg"/> </td>
<td> <img src="screenshots/screenshot2.jpg"/> </td>
</tr></table>