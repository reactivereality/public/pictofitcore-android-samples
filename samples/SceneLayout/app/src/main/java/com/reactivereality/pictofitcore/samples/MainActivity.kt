package com.reactivereality.pictofitcore.samples

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRAvatar
import com.reactivereality.pictofitcore.RRGarment
import com.reactivereality.pictofitcore.rendering.layouts.RRSceneLayout
import com.reactivereality.pictofitcore.rendering.renderables.RRAvatarRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRGarmentRenderable
import com.reactivereality.pictofitcore.rendering.serialization.RRBinarySceneGraphFormat
import com.reactivereality.pictofitcore.rendering.serialization.RRSceneGraphSerializer
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView

class MainActivity : AppCompatActivity() {

  private lateinit var btnSwitchScene : Button
  private lateinit var renderView : RRGLRenderView

  private val sceneLayout = RRSceneLayout.create()!!
  private var sceneIndex = 0
  private val sceneNames = arrayOf("street_girl.arscene", "eiffeltower.arscene")

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    btnSwitchScene = findViewById(R.id.btnSwitchScene)
    renderView = findViewById(R.id.renderView)

    // load garments and avatars, create their respective renderables and add them to the layout
    val avatar = RRAvatar.createFromAsset("avatars/hugo.avatar")!!
    val avatarRenderable = RRAvatarRenderable.createWithAvatar(avatar)!!
    sceneLayout.avatarRenderable = avatarRenderable

    val garment1 = RRGarment.createFromAsset("garments/shirt.garment")!!
    val garment1Renderable = RRGarmentRenderable.createWithGarment(garment1)!!
    val garment2 = RRGarment.createFromAsset("garments/pants2.garment")!!
    val garment2Renderable = RRGarmentRenderable.createWithGarment(garment2)!!
    val garment3 = RRGarment.createFromAsset("garments/jacket.garment")!!
    val garment3Renderable = RRGarmentRenderable.createWithGarment(garment3)!!
    sceneLayout.garmentRenderables = arrayListOf(
        garment1Renderable, garment2Renderable, garment3Renderable
    )

    btnSwitchScene.setOnClickListener { switchScene() }
    renderView.setLayout(sceneLayout)
    switchScene()
  }

  private fun switchScene() {
    sceneIndex = (sceneIndex + 1) % sceneNames.size
    val binarySceneGraphProvider = RRBinarySceneGraphFormat.create()!!
    val serializer = RRSceneGraphSerializer.createWithSceneGraphFormat(binarySceneGraphProvider)!!
    renderView.removeAllRenderables()
    serializer.deserializeFromAsset("scenes/" + sceneNames[sceneIndex], renderView)
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}