# SlotLayout Sample Application

This sample shows how to easily organize 2D objects and try-ons in a collage-like manner within a view. This can be handy to present outfits including accessories.

This sample application includes code snippets showing how to implement the following tasks using the PictofitCore Android SDK:
* Loading an `RRAvatar` from file and creating an `RRAvatarRenderable` from that
* Creating an `RRQuadRenderable` instance from a PNG image file
* Using the `RRSlotLayout` to place the `RRAvatar` and a shadow from a PNG image as a `RRQuadRenderable` under its feet

## Screenshots
<table><tr>
<td> <img src="screenshots/screenshot1.jpg"/> </td>
</tr></table>