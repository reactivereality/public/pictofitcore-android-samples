package com.reactivereality.pictofitcore.samples

import android.graphics.RectF
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRAvatar
import com.reactivereality.pictofitcore.RRGarment
import com.reactivereality.pictofitcore.common.RRImage
import com.reactivereality.pictofitcore.rendering.RRScalingMode
import com.reactivereality.pictofitcore.rendering.layouts.RRSlotLayout
import com.reactivereality.pictofitcore.rendering.renderables.*
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView

class MainActivity : AppCompatActivity() {

  private lateinit var renderView: RRGLRenderView

  private val slotLayout = RRSlotLayout.create()!!
  private val tryOnSlot = RRTryOnSlot.create()!!
  private val shadowSlot = RRQuadSlot.create()!!
  private val accessorySlot1 = RRGarmentSlot.create()!!
  private val accessorySlot2 = RRGarmentSlot.create()!!

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    renderView = findViewById(R.id.renderView)

    val avatar = RRAvatar.createFromAsset("johanna.avatar")!!
    val avatarRenderable = RRAvatarRenderable.createWithAvatar(avatar)!!
    avatarRenderable.backgroundHidden = true

    val garment1 = RRGarment.createFromAsset("pants.garment")!!
    val garment1Renderable = RRGarmentRenderable.createWithGarment(garment1)!!
    val garment2 = RRGarment.createFromAsset("tshirt.garment")!!
    val garment2Renderable = RRGarmentRenderable.createWithGarment(garment2)!!
    tryOnSlot.avatarRenderable = avatarRenderable
    tryOnSlot.garmentRenderables = arrayListOf(
        garment1Renderable, garment2Renderable
    )

    tryOnSlot.zoomToAvatar = false
    tryOnSlot.scalingMode = RRScalingMode.SCALE_TO_HEIGHT

    val shadowImage = RRImage.createFromAsset("shadow.png")!!
    val shadowRenderable = RRQuadRenderable.createWithImage(shadowImage)!!
    shadowSlot.quadRenderable = shadowRenderable
    shadowSlot.scalingMode = RRScalingMode.SCALE_TO_HEIGHT

    val accessory1 = RRGarment.createFromAsset("bag.garment")!!
    val accessory1Renderable = RRGarmentRenderable.createWithGarment(accessory1)!!
    accessorySlot1.garmentRenderable = accessory1Renderable
    accessorySlot1.scalingMode = RRScalingMode.ASPECT_FIT

    val accessory2 = RRGarment.createFromAsset("shoes.garment")!!
    val accessory2Renderable = RRGarmentRenderable.createWithGarment(accessory2)!!
    accessorySlot2.garmentRenderable = accessory2Renderable
    accessorySlot2.scalingMode = RRScalingMode.ASPECT_FIT

    renderView.setLayout(slotLayout)

    // adjust the slot frames after the renderView was loaded as getWidth() etc are only valid after the renderViews initialization is finished
    renderView.post {

      slotLayout.slots = arrayListOf(
          shadowSlot, tryOnSlot, accessorySlot1, accessorySlot2
      )

      val thirdScreenWidth = renderView.width / 3f
      val halfScreenHeight = renderView.height / 2f
      val tryOnRect = RectF(0F, 0F, thirdScreenWidth * 2f, renderView.height.toFloat())
      tryOnSlot.frame = tryOnRect
      shadowSlot.frame = tryOnRect
      accessorySlot1.frame = RectF(thirdScreenWidth * 2f, 0F, renderView.width.toFloat(), halfScreenHeight)
      accessorySlot2.frame = RectF(thirdScreenWidth * 2f, halfScreenHeight, renderView.width.toFloat(), renderView.height.toFloat())
    }
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}