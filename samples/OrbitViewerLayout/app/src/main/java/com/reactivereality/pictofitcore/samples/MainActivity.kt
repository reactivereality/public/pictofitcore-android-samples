package com.reactivereality.pictofitcore.samples

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRGarment3D
import com.reactivereality.pictofitcore.objectdatafileformat.RRTexturedMesh3DObject
import com.reactivereality.pictofitcore.rendering.layouts.RROrbitViewerLayout
import com.reactivereality.pictofitcore.rendering.renderables.RRGarment3DRenderable
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

  private lateinit var renderView : RRGLRenderView

  // init the layout
  private val layout = RROrbitViewerLayout.create()!!

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    renderView = findViewById(R.id.renderView)

    renderView.setLayout(layout)

    // load the renderable / preview into the scene
    GlobalScope.launch(Dispatchers.IO) {
      loadPreviewAsync()
      loadGarmentAsync()
    }
  }

  private fun loadPreviewAsync() {
    // create the garment preview model and add it
    // this is a low resolution model and will be replaced by the actual high quality garment once it finished loading
    val previewModel = RRTexturedMesh3DObject.createFromAsset("accessory_bag_black_preview.tm3d")!!
    val previewRenderable = previewModel.createRenderable()
    layout.renderable = previewRenderable
  }

  private fun loadGarmentAsync() {
    // the real garment might take longer to load
    val garment3D = RRGarment3D.createFromAsset("accessory_bag_black.gm3d")!!
    val garment3DRenderable = RRGarment3DRenderable.createWithGarment(garment3D)
    // when it is done, replace the previously loaded preview with it
    layout.renderable = garment3DRenderable
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}