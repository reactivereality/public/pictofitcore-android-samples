/*
 * Copyright © 2014-2020 Reactive Reality. All rights reserved.
 */

package com.reactivereality.pictofitcore.samples

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.ar.core.ArCoreApk
import com.google.ar.core.ArCoreApk.InstallStatus
import com.google.ar.core.Frame
import com.google.ar.core.Plane
import com.google.ar.core.exceptions.*
import com.reactivereality.pictofitcore.data.RRMatrix4x4
import com.reactivereality.pictofitcore.mesh.RRMesh3D
import com.reactivereality.pictofitcore.rendering.RRTransformation
import com.reactivereality.pictofitcore.rendering.renderables.RRMeshRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRRenderable
import com.reactivereality.pictofitcore.views.events.RRARViewListener
import com.reactivereality.pictofitcore.views.gl.RRARView
import java.nio.FloatBuffer
import java.util.*

class MainActivity : AppCompatActivity(), RRARViewListener {
  private var installRequested = false
  lateinit var arView: RRARView

  // holds all planes arcore has created, that does include planes that consist of other planes
  private val allPlanes: MutableSet<Plane> = HashSet()

  // only holds standalone or top level planes that sum up two or more planes
  private val planeDataList = ArrayList<PlaneData>()
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)
    arView = findViewById(R.id.viewAR)
    // register the listener for the frame update callback
    arView.listener = this
    // register listener to handle touch events
    val touchListener = CustomTouchEventListener(this)
    arView.renderView.setOnTouchListener(touchListener)

    findViewById<Button>(R.id.btnRemove).setOnClickListener { btn ->
      touchListener.dragAndDropHandler.renderable?.let { renderable ->
        touchListener.dragAndDropHandler.unselectRenderable()
        touchListener.removePlacedRenderable(renderable)
        btn.visibility = View.INVISIBLE
      }
    }
  }

  public override fun onResume() {
    super.onResume()

    // check if the arcore session has been started yet
    if (!arView.isSessionRunning) {
      try {
        // ARCore requires camera permissions to operate. If we did not yet obtain runtime
        // permission on Android M and above, now is a good time to ask the user for it.
        if (!PermissionHelper.hasPermissions(this)) {
          PermissionHelper.requestPermissions(this)
          if (!PermissionHelper.hasPermissions(this))
            return
        }

        // if it hasn't we request an install of the Google Play AR Services, as they are mandatory to run an AR app
        if (!checkArCoreInstallation()) return

        // Create the session.
        arView.createSession()
      } catch (ex: UnavailableApkTooOldException) {
        ex.printStackTrace()
      } catch (ex: UnavailableDeviceNotCompatibleException) {
        ex.printStackTrace()
      } catch (ex: UnavailableUserDeclinedInstallationException) {
        ex.printStackTrace()
      } catch (ex: UnavailableArcoreNotInstalledException) {
        ex.printStackTrace()
      } catch (ex: UnavailableSdkTooOldException) {
        ex.printStackTrace()
      }
    }

    // handle lifecycle events
    arView.onResume()
  }

  /**
   * @return `false` if arcore is not installed
   */
  private fun checkArCoreInstallation(): Boolean {
    try {
      return when (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
        InstallStatus.INSTALL_REQUESTED -> {
          installRequested = true
          // force a rerun of the app once arcore is installed
          false
        }
        InstallStatus.INSTALLED -> {
          arView.createSession()
          true
        }
      }
    } catch (ex: Exception) {
      Toast.makeText(applicationContext, "ARCore Google Play services could not be installed!", Toast.LENGTH_LONG).show()
      this.finish()
    }
    return false
  }

  public override fun onPause() {
    super.onPause()
    // handle lifecycle events
    arView.onPause()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, results: IntArray) {
    if (!PermissionHelper.hasPermissions(this)) {
      Toast.makeText(this, "Some permissions are needed to run this application!", Toast.LENGTH_LONG)
          .show()
      if (!PermissionHelper.shouldShowRequestPermissionRationale(this)) {
        // Permission denied with checking "Do not ask again"
        PermissionHelper.launchPermissionSettings(this)
      }
      finish()
    }
  }

  override fun onFrameUpdated(frame: Frame) {
    // get all updates planes
    val allUpdatedPlanes: Set<Plane> = HashSet(frame.getUpdatedTrackables(Plane::class.java))

    // split them up into new planes and planes that not new but got updates
    // this way we do not create the respective renderables every frame update, but instead just update their geometry
    val newPlanes: MutableSet<Plane> = HashSet(allUpdatedPlanes)
    newPlanes.removeAll(allPlanes)
    val updatedExistingPlanes: MutableSet<Plane> = HashSet(allUpdatedPlanes)
    updatedExistingPlanes.removeAll(newPlanes)
    allPlanes.addAll(allUpdatedPlanes)
    for (plane in updatedExistingPlanes) {
      updatePlane(plane)
    }
    for (plane in newPlanes) {
      addNewPlane(plane)
    }
  }

  @SuppressLint("NewApi")
  fun findPlaneDataByRenderable(renderable: RRRenderable): PlaneData? {
    try {
      return planeDataList.stream().filter { x: PlaneData -> x.renderable == renderable }.findFirst().get()
    } catch (ignored: Exception) {
    }
    return null
  }

  // whenever a completely new frame was added, call this
  private fun addNewPlane(plane: Plane) {
    // create a new renderable from the plane information we got from arcore
    val mesh3d = RRMesh3D.createFromARPlane(plane) ?: return

    // sometimes a plane returned by arcore has an invalid geometry, if that is case it will return null
    val planeRenderable = RRMeshRenderable.createWithMesh(mesh3d)!!
    val planeMatrix = FloatArray(16)
    plane.centerPose.toMatrix(planeMatrix, 0)
    val transformation = RRTransformation.createWithMatrix(RRMatrix4x4(planeMatrix).transposed())!!
    planeRenderable.transformation = transformation
    // the first two digits here set the transparency of the renderable
    // 0x10... would mean 10% in this case
    planeRenderable.setColor(0x10669900)
    arView.renderView.addRenderable(planeRenderable)

    // add them to a list so we can perform checks on whether a plane is new or an existing changed one
    planeDataList.add(PlaneData(plane, planeRenderable, plane.polygon))
  }

  // whenever a plane is not new and has just been updated (geometry), call this
  private fun updatePlane(plane: Plane) {
    // update renderable with new mesh
    val data = findPlaneDataByPlane(plane) ?: return

    // arcore merges planes together on the go, therefore a plane might have multiple "child" planes that merged together to from
    // a "masterplane". We just want to render the top-levele masterplanes, as it makes more sense
    // if an update of a sub-plane is called ignore it, because the planeRenderableMap should just store top level planes
    val masterPlane = plane.subsumedBy
    // if plane got is now a child of a bigger plane remove it and add the master plane instead if it isn't already part of the map
    if (masterPlane != null) {
      arView.renderView.removeRenderable(data.renderable)
      planeDataList.remove(data)
      findPlaneDataByPlane(masterPlane)?.let {
        addNewPlane(it.plane)
      }
    }
    else {
      val planeMatrix = FloatArray(16)
      plane.centerPose.toMatrix(planeMatrix, 0)
      val transformation = RRTransformation.createWithMatrix(RRMatrix4x4(planeMatrix).transposed())!!
      data.renderable.transformation = transformation

      // check if the geometry of the plane has changed
      if (plane.polygon != data.polygonData) {
        val mesh3d = RRMesh3D.createFromARPlane(plane) ?: return
        data.renderable.setMesh3D(mesh3d)

        // replace the old object in the list with the new one
        val index = planeDataList.indexOf(data)
        planeDataList[index] = PlaneData(data.plane, data.renderable, data.polygonData)
      }
    }
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }

  private fun findPlaneDataByPlane(plane: Plane): PlaneData? {
    return planeDataList.find { it.plane == plane }
  }

  data class PlaneData(val plane: Plane, val renderable: RRMeshRenderable, val polygonData: FloatBuffer)
}