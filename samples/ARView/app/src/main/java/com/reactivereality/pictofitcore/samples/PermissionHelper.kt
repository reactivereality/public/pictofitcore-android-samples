/*
 * Copyright © 2014-2020 Reactive Reality. All rights reserved.
 */

package com.reactivereality.pictofitcore.samples

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionHelper {
  private const val PERMISSION_CODE = 0
  private const val CAMERA_PERMISSION = Manifest.permission.CAMERA

  fun hasPermissions(activity: Activity?): Boolean {
    return (ContextCompat.checkSelfPermission(activity!!, CAMERA_PERMISSION)
        == PackageManager.PERMISSION_GRANTED)
  }

  fun requestPermissions(activity: Activity?) {
    ActivityCompat.requestPermissions(
        activity!!, arrayOf(CAMERA_PERMISSION), PERMISSION_CODE)
  }

  fun shouldShowRequestPermissionRationale(activity: Activity?): Boolean {
    return ActivityCompat.shouldShowRequestPermissionRationale(activity!!, CAMERA_PERMISSION)
  }

  fun launchPermissionSettings(activity: Activity) {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    intent.data = Uri.fromParts("package", activity.packageName, null)
    activity.startActivity(intent)
  }
}