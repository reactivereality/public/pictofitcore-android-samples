/*
 * Copyright © 2014-2020 Reactive Reality. All rights reserved.
 */

package com.reactivereality.pictofitcore.samples

import android.graphics.PointF
import android.util.Pair
import android.view.GestureDetector
import android.view.GestureDetector.OnDoubleTapListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Button
import android.widget.Toast
import com.google.ar.core.Anchor
import com.google.ar.core.Pose
import com.reactivereality.pictofitcore.data.RRIntersection
import com.reactivereality.pictofitcore.data.RRQuaternion
import com.reactivereality.pictofitcore.data.RRVector3
import com.reactivereality.pictofitcore.rendering.RRCarouselRenderableDataSource
import com.reactivereality.pictofitcore.rendering.RRTransformation
import com.reactivereality.pictofitcore.rendering.renderables.RRCarouselRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRCarouselRenderable.RRCarouselRenderableListener
import com.reactivereality.pictofitcore.rendering.renderables.RRRenderable
import com.reactivereality.pictofitcore.samples.RotationGestureDetector.OnRotationGestureListener
import com.reactivereality.pictofitcore.views.gl.RRARView
import java.util.*

// Handle various touch events
class CustomTouchEventListener(activity: MainActivity) : OnTouchListener, GestureDetector.OnGestureListener, OnDoubleTapListener, OnRotationGestureListener {
  private val mGestureDetector: GestureDetector = GestureDetector(activity, this)
  private val mRotationDetector: RotationGestureDetector
  private val renderViewListener: OnTouchListener

  // this variable holds the scrolling state as android does not return a "OnScrollEnd" we have to create it by ourselfs
  private var isDragging = false

  // since rotation / drag and carousel events run in parallel, we don't want them to interfere
  // therefore we handle carousel events first and set itemSelected as a flag if a carouselEvent was triggered
  // if this is true we ignore any other events in this onTouch callback
  private var itemSelected = false
  private var isCarouselPlaced = false
  private var arView: RRARView
  private val activity: MainActivity

  // hold a list of every placed renderable and its corresponding anchor
  // creating anchors for objects in the scene is best practice, as it stabilizes the ar scene around this points
  // which reduces weird position shifts
  // TODO: these anchors might get updated by a frame update too
  private val placedRenderableAnchorList = ArrayList<Pair<RRRenderable, Anchor>>()

  // the drag and drop handler handles how a selected renderable is transformed in the scene
  var dragAndDropHandler = RenderableDragAndDropHandler()
  override fun onTouch(view: View, event: MotionEvent): Boolean {
    // handle render view events (carousel) only when there is no renderable selected
    if (dragAndDropHandler.renderable == null) {
      renderViewListener.onTouch(view, event)
    }
    mGestureDetector.onTouchEvent(event)
    mRotationDetector.onTouchEvent(event)

    // since the OnGestureListener does not provide an onUp() event callback, we grab it here to detect the end of a scroll
    val action = event.action
    if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
      isDragging = false
      dragAndDropHandler.draggingGestureEnded()
    }
    return true
  }

  override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
    // single taps spawn the carousel
    val firstIntersection = getFirstIntersectionWithPlane(e)
    if (firstIntersection != null && !itemSelected) {
      placeCarousel(firstIntersection)
    }
    else if (!isCarouselPlaced) {
      Toast.makeText(arView.context, R.string.txtNoPlaneFound, Toast.LENGTH_SHORT).show()
    }
    itemSelected = false
    return true
  }

  override fun onDoubleTap(e: MotionEvent): Boolean {
    // change remove item button visibility
    val btnRemove = activity.findViewById<Button>(R.id.btnRemove)

    // double taps select already placed items
    val firstIntersection = getFirstIntersectionWithNonPlane(e)
    // placed renderable was hit
    if (firstIntersection != null) {
      val renderable = firstIntersection.renderable!!
      val selectedRenderableWasSame = dragAndDropHandler.isRenderableSelected(renderable)
      // if its not the previously selected one, select this one
      if (!selectedRenderableWasSame) {
        btnRemove.visibility = View.VISIBLE
        dragAndDropHandler.selectRenderable(renderable)
      }
      else {
        // if its the same, just deselect it
        btnRemove.visibility = View.INVISIBLE
        dragAndDropHandler.unselectRenderable()
      }
    }
    else {
      btnRemove.visibility = View.INVISIBLE
      dragAndDropHandler.unselectRenderable()
    }
    return true
  }

  override fun onDoubleTapEvent(e: MotionEvent): Boolean {
    return false
  }

  override fun onDown(e: MotionEvent): Boolean {
    return false
  }

  override fun onShowPress(e: MotionEvent) {}
  override fun onSingleTapUp(e: MotionEvent): Boolean {
    return false
  }

  override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
    // e1 holds the initial start position of the scroll
    // therefore we fire it only once when the scroll event is first called
    if (!isDragging) {
      isDragging = true
      val firstIntersection = getFirstIntersectionWithNonPlane(e1)
      val touchPosition = PointF(e1.x, e1.y)
      val ray = arView.renderView.rayFromViewPosition(touchPosition)
      if (firstIntersection != null && firstIntersection.renderable == dragAndDropHandler.renderable) {
        dragAndDropHandler.draggingGestureBegan(ray, firstIntersection)
      }
    }
    // offer scroll update events for the new position of the scroll (e2)
    if (isDragging) {
      val touchPosition = PointF(e2.x, e2.y)
      val ray = arView.renderView.rayFromViewPosition(touchPosition)
      dragAndDropHandler.draggingGestureMoved(ray)
    }
    return true
  }

  override fun onLongPress(e: MotionEvent) {}
  override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
    return false
  }

  override fun onRotationBegan(angle: Float) {
    dragAndDropHandler.rotationGestureBegan()
  }

  override fun onRotationChanged(angle: Float) {
    dragAndDropHandler.rotationGestureMoved(angle)
  }

  override fun onRotationCanceled(angle: Float) {
    dragAndDropHandler.rotationGestureEnded()
  }

  /**
   * private helper methods
   */
  private fun getFirstIntersectionWithPlane(touchEvent: MotionEvent): RRIntersection? {
    val touchPosition = PointF(touchEvent.x, touchEvent.y)
    val ray = arView.renderView.rayFromViewPosition(touchPosition)
    val intersections = arView.renderView.getIntersections(ray)
    for (intersection in intersections) {
      if (isPlaneRenderable(intersection.renderable!!) && intersection.distance > 0.01f) {
        return intersection
      }
    }
    return null
  }

  private fun getFirstIntersectionWithNonPlane(event: MotionEvent): RRIntersection? {
    val touchPosition = PointF(event.x, event.y)
    val ray = arView.renderView.rayFromViewPosition(touchPosition)
    val intersections = arView.renderView.getIntersections(ray)
    for (intersection in intersections) {
      if (intersection.distance > 0.01f) {
        if (!isPlaneRenderable(intersection.renderable!!)) {
          return intersection
        }
      }
    }
    return null
  }

  // creates an anchor at the current position of the renderable and adds it to the list
  private fun placeRenderable(renderable: RRRenderable) {
    // Cap the number of objects that can be created. This avoids overloading both the rendering system and ARCore.
    if (placedRenderableAnchorList.size >= 20) {
      placedRenderableAnchorList[0].second!!.detach()
      arView.renderView.removeRenderable(placedRenderableAnchorList[0].first)
      placedRenderableAnchorList.removeAt(0)
    }
    arView.renderView.addRenderable(renderable)

    // it is needed to create an anchor from the intersection position to avoid arcore loosing track of its position
    val newPose = Pose(renderable.transformation.translation.toArray(), RRQuaternion.IDENTITY.toArray())
    val newAnchor = arView.arSession.createAnchor(newPose)
    placedRenderableAnchorList.add(Pair<RRRenderable, Anchor>(renderable, newAnchor))
  }

  // removes an existing renderable from the list and detaches its anchor
  fun removePlacedRenderable(renderable: RRRenderable?) {
    if (renderable != null) {
      for (pair in placedRenderableAnchorList) {
        val currentRenderable = pair.first as RRRenderable
        val anchor = pair.second as Anchor
        if (renderable == currentRenderable) {
          anchor.detach()
          placedRenderableAnchorList.remove(pair)
          break
        }
      }
      renderable.removeFromParent()
      arView.renderView.removeRenderable(renderable)
    }
  }

  private fun isPlaneRenderable(renderable: RRRenderable): Boolean {
    return activity.findPlaneDataByRenderable(renderable) != null
  }

  var carouselRenderable: RRCarouselRenderable? = null

  private fun placeCarousel(intersection: RRIntersection) {
    if (dragAndDropHandler.renderable != null) {
      return
    }
    if (carouselRenderable != null) {
      removePlacedRenderable(carouselRenderable)
    }
    carouselRenderable = RRCarouselRenderable.create()
    val transformation = RRTransformation.create()!!
    // add some offset here for the spinning image because otherwise it would get overshadowed by the plane mesh
    transformation.translation = intersection.intersectionPoint!!.add(RRVector3(0F, 0.05F, 0F))
    carouselRenderable!!.transformation = transformation
    carouselRenderable!!.minimumItemScaleAngularDistance = 30.0f
    carouselRenderable!!.minimumItemScale = 0.3f
    carouselRenderable!!.angularItemsDistance = 25.0f
    carouselRenderable!!.dataSource = RRCarouselRenderableDataSource.createWithInstance(ARObjectsDataSource())
    carouselRenderable!!.setListener(object : RRCarouselRenderableListener {
      override fun itemWasSelectedAtIndex(carouselRenderable: RRCarouselRenderable?, index: Int) {
        itemSelected = true
        removePlacedRenderable(carouselRenderable)
        val dataSource = carouselRenderable!!.dataSource as ARObjectsDataSource
        val itemRenderable = dataSource.getFullSizeRenderable(index, carouselRenderable.transformation)
        placeRenderable(itemRenderable)
      }

      override fun carouselRegisteredTapOutsideBoundingBox(carouselRenderable: RRCarouselRenderable?) {
        // unspawn the carousel from the scene if it wasnt effected by a touch
        removePlacedRenderable(carouselRenderable)
        isCarouselPlaced = false
      }
    })
    isCarouselPlaced = true
    placeRenderable(carouselRenderable!!)
  }

  init {
    mGestureDetector.setOnDoubleTapListener(this)
    mRotationDetector = RotationGestureDetector(this)
    this.activity = activity
    arView = activity.arView
    renderViewListener = arView.renderView.defaultOnTouchListener
  }
}