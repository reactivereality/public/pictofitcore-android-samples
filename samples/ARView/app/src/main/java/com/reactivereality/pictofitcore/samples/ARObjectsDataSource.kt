/*
 * Copyright © 2014-2020 Reactive Reality. All rights reserved.
 */

package com.reactivereality.pictofitcore.samples

import com.reactivereality.pictofitcore.RRAvatar3D
import com.reactivereality.pictofitcore.RRGarment3D
import com.reactivereality.pictofitcore.common.RRImage
import com.reactivereality.pictofitcore.data.RRMatrix4x4
import com.reactivereality.pictofitcore.data.RRVector3
import com.reactivereality.pictofitcore.objectdatafileformat.RRTexturedMesh3DObject
import com.reactivereality.pictofitcore.rendering.RRCarouselRenderableDataSource
import com.reactivereality.pictofitcore.rendering.RRTransformation
import com.reactivereality.pictofitcore.rendering.renderables.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.*

// enum to distinguish between previews which we use a TexturedMesh3D for and the actual Garment3D / Avatar3D
enum class ModelType {
  TEXTUREDMESH_3D,
  AVATAR_3D,
  GARMENT_3D,
  UNKNOWN
}

// POJO to hold important data about a single carousel item
private class CarouselItem(var meshName: String, var previewMeshName: String)

class ARObjectsDataSource : RRCarouselRenderableDataSource() {

  // a list that holds all item that this datasource offers
  private val carouselItems: ArrayList<CarouselItem> = arrayListOf(
      CarouselItem(
          "garments/accessory_bag_black.gm3d",
          "garments/accessory_bag_black_preview.tm3d"
      ),
      CarouselItem(
          "garments/accessory_bag_blue.gm3d",
          "garments/accessory_bag_blue_preview.tm3d"
      ),
      CarouselItem(
          "garments/accessory_female_shoe.gm3d",
          "garments/accessory_female_shoe_preview.tm3d"
      ),
      CarouselItem(
          "garments/accessory_hat_black.gm3d",
          "garments/accessory_hat_black_preview.tm3d"
      ),
      CarouselItem(
          "garments/accessory_male_boot.gm3d",
          "garments/accessory_male_boot_preview.tm3d"
      ),
      CarouselItem(
          "garments/female_blouse_blue.gm3d",
          "garments/female_blouse_blue_preview.tm3d"
      ),
      CarouselItem(
          "garments/female_dress_blue.gm3d",
          "garments/female_dress_blue_preview.tm3d"
      ),
      CarouselItem(
          "garments/female_shirt_blue.gm3d",
          "garments/female_shirt_blue_preview.tm3d"
      ),
      CarouselItem(
          "garments/male_jeans_blue.gm3d",
          "garments/male_jeans_blue_preview.tm3d"
      ),
      CarouselItem(
          "garments/male_suit_jacket_blue.gm3d",
          "garments/male_suit_jacket_blue_preview.tm3d"
      ),
      CarouselItem(
          "avatars/edward_2_compressed.av3d",
          "avatars/edward_2_preview.tm3d"
      ),
      CarouselItem(
          "avatars/floriane_3_compressed.av3d",
          "avatars/floriane_3_preview.tm3d"
      )
  )

  // an image that will act as a placeholder / loading indicator while an item is being loaded
  private var spinningWheelImage: RRImage = RRImage.createFromAsset("images/SpinningWheel.png")!!

  private fun loadRenderableSync(fileName: String): RRRenderable? {
    return when (getModelTypeForFileName(fileName)) {
      ModelType.TEXTUREDMESH_3D -> {
        val texturedMesh = RRTexturedMesh3DObject.createFromAsset(fileName)!!
        texturedMesh.createRenderable()
      }
      ModelType.GARMENT_3D -> {
        val garment = RRGarment3D.createFromAsset(fileName)!!
        RRGarment3DRenderable.createWithGarment(garment)
      }
      ModelType.AVATAR_3D -> {
        val avatar = RRAvatar3D.createFromAsset(fileName)!!
        RRAvatar3DRenderable.createWithAvatar(avatar)
      }
      else -> null
    }
  }

  private fun getModelTypeForFileName(fileName: String): ModelType {
    return when (File(fileName).extension) {
      "tm3d" -> ModelType.TEXTUREDMESH_3D
      "gm3d" -> ModelType.GARMENT_3D
      "av3d" -> ModelType.AVATAR_3D
      else -> ModelType.UNKNOWN
    }
  }

  // calculate the preview model scale and translation according to the its bounding box
  private fun calculatePreviewModelTransformation(renderable: RRRenderable): RRTransformation {
    val boundingBox = renderable.getBoundingBoxLocal(false)!!
    val lowerBoundingBoxCenter = boundingBox.center.toArray()
    lowerBoundingBoxCenter[1] = boundingBox.minimum.toArray()[1]
    var scale = Float.MAX_VALUE
    val maximumBoundingBoxSize = floatArrayOf(0.2f, 0.4f, 0.2f)
    val boundingBoxMax = boundingBox.maximum.toArray()
    val boundingBoxMin = boundingBox.minimum.toArray()
    for (i in 0 until RRVector3.DIMENSION) {
      val tempScale = maximumBoundingBoxSize[i] / (boundingBoxMax[i] - boundingBoxMin[i])
      scale = java.lang.Float.min(scale, tempScale)
    }
    val result = RRTransformation.create()!!
    result.scale = RRVector3(scale)
    result.translation = RRVector3(lowerBoundingBoxCenter).multiply(-1f).multiply(scale)
    return result
  }

  // rotate the loading image to be in the XZ plane instead of its initial XY plane
  private fun getXZImageRotationTransformation(size: Float): RRTransformation {
    val rotationMatrixTransposed = RRMatrix4x4(floatArrayOf(
        1f, 0f, 0f, 0f,
        0f, 0f, -1f, 0f,
        0f, 1f, 0f, 0f,
        0f, 0f, 0f, 0f
    ))
    val rotationMatrix = rotationMatrixTransposed.transposed()
    val transformation = RRTransformation.createWithMatrix(rotationMatrix)!!
    transformation.scale = RRVector3(size, size, size)
    return transformation
  }

  // load a model via a thread
  // while the model is not loaded it will show a loading image
  private fun getAsyncLoadedModel(meshFileName: String, spinningWheelSize: Float, modelTransformation: RRTransformation?): RRRenderable {
    val isPreview = modelTransformation == null

    val spinningWheelRenderable = RRSpinningImageRenderable.createWithImage(spinningWheelImage)!!
    val spinningWheelTransformation = getXZImageRotationTransformation(spinningWheelSize)
    // move spinning image if its not a preview
    if (!isPreview) {
      spinningWheelTransformation.translation = modelTransformation!!.translation
    }
    spinningWheelRenderable.transformation = spinningWheelTransformation
    spinningWheelRenderable.numberOfDiscretizedRotationSteps = 8


    val parent: RRRenderable = RRHoveringRenderable.createWithAutomaticLifting(false, false)!!
    parent.addChild(spinningWheelRenderable)

    GlobalScope.launch(Dispatchers.IO) {
      val renderable = loadRenderableSync(meshFileName)?.apply {
        transformation = if (isPreview)
          calculatePreviewModelTransformation(this)
        else modelTransformation!!
      }
      parent.addChild(renderable!!)
      spinningWheelRenderable.removeFromParent()
    }

    return parent
  }

  fun getFullSizeRenderable(index: Int, spawnTransformation: RRTransformation?): RRRenderable {
    val modelIndex = index % carouselItems.size
    val meshName = carouselItems[modelIndex].meshName
    return getAsyncLoadedModel(meshName, 0.3f, spawnTransformation)
  }

  // override nessecary functions of the RRCarouselRenderableDataSource

  override fun getNumberOfCarouselItems(carousel: RRCarouselRenderable): Int {
    return carouselItems.size
  }

  // this will be called in the core to know which items to render in the carousel itself
  override fun getCarouselItemRenderableAtIndex(carousel: RRCarouselRenderable, itemIndex: Int): RRRenderable? {
    val modelIndex = itemIndex % carouselItems.size
    val meshName = carouselItems[modelIndex].previewMeshName
    return getAsyncLoadedModel(meshName, 0.075f, null)
  }
}