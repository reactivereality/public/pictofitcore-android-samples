/*
 * Copyright © 2014-2020 Reactive Reality. All rights reserved.
 */

package com.reactivereality.pictofitcore.samples

import com.reactivereality.pictofitcore.data.RRIntersection
import com.reactivereality.pictofitcore.data.RRRay
import com.reactivereality.pictofitcore.data.RRVector3
import com.reactivereality.pictofitcore.rendering.renderables.RRHoveringRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRRenderable

class RenderableDragAndDropHandler {

  private var rotationAngleGestureBegan = 0f
  private var draggingPlaneY = 0f
  private var draggingStartPosition: RRVector3? = null

  var renderable: RRRenderable? = null
    private set
  var isDragging = false
    private set
  var isRotating = false
    private set

  fun isRenderableSelected(renderable: RRRenderable?): Boolean {
    return this.renderable != null && renderable!!.equals(renderable)
  }

  fun selectRenderable(renderable: RRRenderable) {
    unselectRenderable()
    val parent = renderable.parent!!.parent
    (parent as RRHoveringRenderable).startHovering()
    this.renderable = renderable
  }

  fun unselectRenderable() {
    if (renderable != null) {
      val hoveringRenderable = renderable!!.parent!!.parent as RRHoveringRenderable
      hoveringRenderable.endHovering()
      renderable = null
    }
  }

  fun calculateScrollingPlaneIntersection(ray: RRRay): RRVector3 {
    if (ray.direction!!.y == 0.0f) {
      return RRVector3()
    }
    val t = (draggingPlaneY - ray.origin!!.y) / ray.direction!!.y
    return ray.origin!!.add(ray.direction!!.multiply(t))
  }

  fun draggingGestureBegan(ray: RRRay, intersection: RRIntersection) {
    if (renderable == null || isRotating) {
      return
    }
    draggingPlaneY = intersection.intersectionPoint!!.y
    draggingStartPosition = calculateScrollingPlaneIntersection(ray).subtract(renderable!!.transformation.translation)
    isDragging = true
  }

  fun draggingGestureMoved(ray: RRRay) {
    if (renderable == null || !isDragging) {
      return
    }
    val transformation = renderable!!.transformation
    transformation.translation = calculateScrollingPlaneIntersection(ray).subtract(draggingStartPosition!!)
    renderable!!.transformation = transformation
  }

  fun draggingGestureEnded() {
    isDragging = false
  }

  fun rotationGestureBegan() {
    if (renderable == null || isDragging) {
      return
    }
    val transformation = renderable!!.transformation
    val currentRotation = transformation.rotationAngles.y
    rotationAngleGestureBegan = currentRotation
    isRotating = true
  }

  fun rotationGestureMoved(rotationAngle: Float) {
    if (renderable == null || !isRotating) {
      return
    }
    val transformation = renderable!!.transformation
    val rotation = transformation.rotationAngles
    val newRotation = RRVector3(rotation.x, rotationAngleGestureBegan + rotationAngle, rotation.z)
    transformation.rotationAngles = newRotation
    renderable!!.transformation = transformation
  }

  fun rotationGestureEnded() {
    isRotating = false
  }
}