# ARView Sample Application

This sample shows how to use PictofitCore to load 3d objects from files and place them in an augmented reality scene. 
It also shows how to interactively select 3D objects inside the augmented reality scene.

This sample application includes code snippets showing how to implement the following tasks using the PictofitCore Android SDK:
* Asynchronous loading of different 3D objects from a file
* Creating Renderables to visualize 3D object instances
* Using the `RRARView` class to use the PictofitCore rendering engine in combination with an ARCore session
* Using the `RRMeshRenderable` class to add detected ground planes from ARCore to the hit testing pipeline of the `RRGLRenderView` class
* Interactive selection of 3D renderables in an AR scene using the `RRCarouselRenderable` class
* Placing 3D renderables in the AR scene
* Select and translate/rotate 3D renderables in the AR scene by setting a parent `RRHoveringRenderable`

## Notes:
> Garments return a `RRMeshRenderable` when involved in an intersection which is a child renderable of the actual `Garment3DRenderable`

> Avatars return a `RRUnkownRenderable` when involved in an intersection which is a child renderable of the actual `Avatar3DRenderable`

## Screenshots
<table><tr>
<td> <img src="screenshots/screenshot1.jpg"/> </td>
<td> <img src="screenshots/screenshot2.jpg"/> </td>
</tr></table>