# Offscreen Rendering Sample Application

This sample shows how to use to render the content of the render view to an image without displaying it while doing so.
Android requires a RenderView to present in the scene, therefore we create an almost invisible 1x1 RenderView.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore Android 
SDK:

* Creating a simple 2D try-on scene using the RRUserPhotoLayout class
* Rendering the 2D try-on to an image using the renderToImage method of the RenderView