package com.reactivereality.pictofitcore.samples

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRAvatar
import com.reactivereality.pictofitcore.RRGarment
import com.reactivereality.pictofitcore.rendering.layouts.RRUserPhotoLayout
import com.reactivereality.pictofitcore.rendering.renderables.RRAvatarRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRGarmentRenderable
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView

class MainActivity : AppCompatActivity() {

  private lateinit var imageResult: ImageView
  private lateinit var renderViewOffscreen: RRGLRenderView

  private val layout: RRUserPhotoLayout = RRUserPhotoLayout.create()!!

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    imageResult = findViewById(R.id.imgResult)
    renderViewOffscreen = findViewById(R.id.renderViewOffscreen)

    // init render view
    val avatar = RRAvatar.createFromAsset("martina.avatar")!!
    val avatarRenderable = RRAvatarRenderable.createWithAvatar(avatar)!!.apply {
      backgroundHidden = true
    }

    // wait for the tryOn calculation to be finished, before rendering
    avatarRenderable.setAvatarRenderableListener(object : RRAvatarRenderable.RRAvatarRenderableListener {
      override fun tryonReady(avatarRenderable: RRAvatarRenderable?) {
        // render the result to the imageView
        val bitmap = renderViewOffscreen.renderToImage(imageResult.width, imageResult.height)
        imageResult.setImageBitmap(bitmap)
      }
    })
    layout.avatarRenderable = avatarRenderable
    layout.garmentRenderables = arrayListOf(
        loadGarmentRenderable("blouse"),
        loadGarmentRenderable("pants")
    )
    // disable animations, otherwise we might get a render result while still animating
    layout.animationsEnabled = false

    renderViewOffscreen.setLayout(layout)
  }

  private fun loadGarmentRenderable(name: String): RRGarmentRenderable {
    val garment = RRGarment.createFromAsset("garments/$name.garment")!!
    return RRGarmentRenderable.createWithGarment(garment)!!
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}