package com.reactivereality.pictofitcore.samples

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.reactivereality.pictofitcore.RRAvatar
import com.reactivereality.pictofitcore.RRGarment
import com.reactivereality.pictofitcore.rendering.layouts.RRUserPhotoLayout
import com.reactivereality.pictofitcore.rendering.renderables.RRAvatarRenderable
import com.reactivereality.pictofitcore.rendering.renderables.RRGarmentRenderable
import com.reactivereality.pictofitcore.views.gl.RRGLRenderView

class MainActivity : AppCompatActivity(), OnTouchListener {

  private lateinit var renderView : RRGLRenderView

  // create the layout that manages all renderables
  private val layout: RRUserPhotoLayout = RRUserPhotoLayout.create()!!

  // hold all garments in a list so we can iterate through them
  private lateinit var upperGarments: List<RRGarmentRenderable>
  private lateinit var lowerGarments: List<RRGarmentRenderable>
  private var upperGarmentIndex = 0
  private var lowerGarmentIndex = 0

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    makeFullScreen()
    setContentView(R.layout.activity_main)

    renderView = findViewById(R.id.renderView)

    renderView.setOnTouchListener(this)

    // load the avatar
    val avatar = RRAvatar.createFromAsset("martina.avatar")!!
    // create the corresponding renderable
    val avatarRenderable = RRAvatarRenderable.createWithAvatar(avatar)!!
    avatarRenderable.backgroundHidden = true
    // add the avatar renderable to the layout
    layout.avatarRenderable = avatarRenderable

    // same strategy for garments
    upperGarments = arrayListOf(
        loadGarmentRenderable("blouse"),
        loadGarmentRenderable("sweater"),
        loadGarmentRenderable("tshirt")
    )
    lowerGarments = arrayListOf(
        loadGarmentRenderable("pants"),
        loadGarmentRenderable("pants2")
    )
    // set the layout to the renderView
    renderView.setLayout(layout)

    updateGarmentRenderables()
  }

  private fun updateGarmentRenderables() {
    layout.garmentRenderables = arrayListOf(
        lowerGarments[lowerGarmentIndex],
        upperGarments[upperGarmentIndex]
    )
  }

  private fun loadGarmentRenderable(name: String): RRGarmentRenderable {
    // load garment file and create a renderable from it
    val garment = RRGarment.createFromAsset("garments/$name.garment")!!
    return RRGarmentRenderable.createWithGarment(garment)!!
  }

  override fun onTouch(v: View, event: MotionEvent): Boolean {
    v.performClick()

    if (event.actionMasked != MotionEvent.ACTION_DOWN)
      return false

    val y = event.y
    // change the upper garment type if the user presses on the top half of the screen
    if (y < 0.5 * renderView.height)
      upperGarmentIndex = (upperGarmentIndex + 1) % upperGarments.size
    // otherwise rotate through the lower garment list
    else lowerGarmentIndex = (lowerGarmentIndex + 1) % lowerGarments.size
    updateGarmentRenderables()
    return true
  }

  private fun makeFullScreen() {
    requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
    supportActionBar?.hide()
  }
}