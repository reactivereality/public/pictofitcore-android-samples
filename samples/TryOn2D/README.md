# TryOn2D Sample Application

This sample shows how to easily mix and match 2D garments on a 2D avatar. Providing a large set of garments, this can be seen as a virtual dressing room.

This sample application includes code snippets showing how to implement the following tasks using the PictofitCore Android SDK:
* Loading an `RRAvatar` from a file and creating an `RRAvatarRenderable` from that
* Loading an `RRGarment` from a file and creating an `RRGarmentRenderable` from that
* Using the `RRUserPhotoLayout` to render a 2D try on using an avatar and multiple garments

## Screenshots
<table><tr>
<td> <img src="screenshots/screenshot1.jpg"/> </td>
<td> <img src="screenshots/screenshot2.jpg"/> </td>
</tr></table>